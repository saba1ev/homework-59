import React, {Component, Fragment} from 'react';
import FormJokes from "../Component/FormJokes/FormJokes";

class Jokes extends Component {
  state={
    jokes: [],
  };

  getJoke = () => {
    const URL = 'https://api.chucknorris.io/jokes/random';
    fetch(URL).then(response => {
      if (response.ok) {
        return response.json()

      }
    }).then(response => {
      this.setState({
        jokes: [{id: response.id, text: response.value, img: response.icon_url}]
      })
    })
  };

  componentDidMount(){
    this.getJoke();
  }
  render() {
    return (
      <Fragment>
        {this.state.jokes.map((joke, index)=>{
          return(
            <div  key={joke.id}>
              <FormJokes
                index={index}
                text={joke.text}
                url={joke.img}
              />
            </div>

          )

        })}

        <button onClick={this.getJoke}>Get Joke</button>
      </Fragment>
    );
  }
}

export default Jokes;