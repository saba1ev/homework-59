import React, {Component, Fragment} from 'react';
import './FormJokes.css'

class FormJokes extends Component {
  render() {
    return (
      <Fragment>
        <img src={this.props.url} alt=""/>
        <p>{this.props.text}</p>
      </Fragment>
    );
  }
}

export default FormJokes;