import React, {Component, Fragment} from 'react';
import './App.css';
import Jokes from "./Container/jokes";

class App extends Component {
  render() {
    return (
     <Fragment>
        <Jokes/>
     </Fragment>
    );
  }
}

export default App;
